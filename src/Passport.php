<?php
namespace Idesigning\Passports;

class Passport
{
    public static function isInValid($series, $number)
    {
        $pass = $series . ',' . $number;
        $fn = fopen(__DIR__ . '/../data/passports.dat', 'rb');

        $seria = pack('n', substr($pass, 0, 4));
        $num = substr($pass, 5);

        fseek($fn, $num * 4);
        $seek = unpack('Nbegin/Nend', fread($fn, 8));

        fseek($fn, $seek['begin']);
        $series = fread($fn, $seek['end'] - $seek['begin']);

        return ($pos = strpos($series, $seria)) !== false && $pos % 2 == 0;
    }

    public static function isValid($series, $number)
    {
        return !self::isInValid($series, $number);
    }
}